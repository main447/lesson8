import java.util.HashSet;
import java.util.Iterator;

public class Task8 {
    HashSet<String> set = new HashSet<String>();

    public void add(String val) {
        set.add(val);
    }
    public boolean contains(String obj){
        return set.contains(obj);
    }
    public boolean isEmpty(){
        return set.isEmpty();
    }
    public void remove(Object obj){
        set.remove(obj);
    }
    public int size(){
        return set.size();
    }
    public Iterator iterator() {
        return set.iterator();
    }

}
